import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import AddSimpsonScreen from '../screens/AddSimpsonScreen';
import SimpsonDetailScreen from '../screens/SimpsonDetailScreen';

const Stack = createStackNavigator();

export default function MainNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{title: 'Anasayfa'}}
        />
        <Stack.Screen
          name="AddSimpson"
          component={AddSimpsonScreen}
          options={{title: 'Simpson Ekle'}}
        />
        <Stack.Screen
          name="SimpsonDetail"
          component={SimpsonDetailScreen}
          options={{title: 'Simpson Detay'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
