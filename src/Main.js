import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import MainNavigator from './navigation/MainNavigator';

export default function Main() {
  return <MainNavigator />;
}
