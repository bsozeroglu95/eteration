import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {icons} from '../theme/icons';
import {connect} from 'react-redux';
import {listSave} from '../redux/actions/simpsons';
import _ from 'lodash';
function RenderSimpsons(props) {
  const {item, onPress} = props;

  const deleteItem = () => {
    var list = _.cloneDeep(props.simpsons);
    var found = list.findIndex(function (element) {
      return element.id === item.id;
    });
    list.splice(found, 1);
    props.onDeleteItem(list);
  };

  return (
    <TouchableOpacity
      onPress={() => onPress()}
      style={{
        flex: 1,
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View style={{width: 40, height: 40, borderRadius: 20}}>
          <Image
            source={{uri: item.avatar}}
            style={{width: null, height: null, flex: 1, resizeMode: 'contain'}}
          />
        </View>
        <Text>{item.name}</Text>
      </View>
      <TouchableOpacity onPress={() => deleteItem()}>
        <Image source={icons.trash} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

const mapStateToProps = (state) => {
  return {
    simpsons: state.simpsons.simpsonsList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteItem: (data) => dispatch(listSave(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RenderSimpsons);
