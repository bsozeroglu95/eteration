import React from 'react';
import {View, Text, ScrollView, Image, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export default function SimpsonDetailScreen(props) {
  const {item} = props.route.params;
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1, padding: 10}}>
      <Image
        source={{uri: item.avatar}}
        style={{width: width - 20, height: 300, resizeMode: 'contain'}}
      />
      <View
        style={{padding: 10, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontWeight: 'bold', fontSize: 28}}>{item.name}</Text>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 18, color: 'gray'}}>{item.job}</Text>
      </View>
      <View style={{padding: 10}}>
        <Text style={{fontSize: 16}}>{item.about}</Text>
      </View>
    </ScrollView>
  );
}
