import React, {useState} from 'react';
import {TextInput as NativeTextInput, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextInput, Button} from 'react-native-paper';
import {listSave} from '../redux/actions/simpsons';
import {connect} from 'react-redux';
import _ from 'lodash';

const theme = {
  roundness: 2,
  colors: {
    primary: 'black',
    accent: 'white',
  },
};

function AddSimpsonScreen(props) {
  const [name, setName] = useState('');
  const [job, setJob] = useState('');
  const [about, setAbout] = useState('');
  const [image, setİmage] = useState('');

  const saveCharacter = () => {
    const list = _.cloneDeep(props.simpsons);
    var id = 1;
    list.forEach((item) => {
      if (item.id === id.toString()) {
        id = id + 1;
      }
    });
    var data = {
      id: id.toString(),
      name: name,
      avatar: image,
      job: job,
      about: about,
    };
    list.push(data);
    props.onSaveList(list);
    props.navigation.pop();
  };

  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="always"
      contentContainerStyle={{flexGrow: 1, padding: 10}}>
      <TextInput
        style={{marginVertical: 5}}
        mode="outlined"
        label="Name"
        onChangeText={(text) => setName(text)}
        value={name}
        theme={theme}
      />
      <TextInput
        style={{marginVertical: 5}}
        mode="outlined"
        label="Job Title"
        onChangeText={(text) => setJob(text)}
        value={job}
        theme={theme}
      />
      <TextInput
        style={{marginVertical: 5}}
        mode="outlined"
        label="About Him/Her"
        textAlignVertical="top"
        onChangeText={(text) => setAbout(text)}
        value={about}
        theme={theme}
        render={(innerProps) => (
          <NativeTextInput
            {...innerProps}
            style={[
              innerProps.style,
              {
                textAlignVertical: 'top',
                height: 200,
              },
            ]}
          />
        )}
      />
      <TextInput
        style={{marginVertical: 5}}
        mode="outlined"
        label="Image Link"
        onChangeText={(text) => setİmage(text)}
        value={image}
        theme={theme}
      />
      <View style={{marginTop: 10}}>
        <Button
          onPress={() => saveCharacter()}
          mode="contained"
          color="lightblue">
          Ekle
        </Button>
      </View>
    </KeyboardAwareScrollView>
  );
}
const mapStateToProps = (state) => {
  return {
    simpsons: state.simpsons.simpsonsList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSaveList: (data) => dispatch(listSave(data)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddSimpsonScreen);
