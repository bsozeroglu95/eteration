import React, {useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import RenderSimpsons from '../components/RenderSimpsons';
import {getList} from '../redux/actions/simpsons';
import _ from 'lodash';
import {icons} from '../theme/icons';

function HomeScreen(props) {
  useEffect(() => {
    if (_.isEmpty(props.simpsons)) {
      props.onGetList();
    }
  }, []);

  return (
    <View style={{flex: 1}}>
      <FlatList
        contentContainerStyle={{paddingBottom: 25, flexGrow: 1}}
        data={props.simpsons}
        ItemSeparatorComponent={() => {
          return <View style={{height: 1, backgroundColor: 'black'}} />;
        }}
        keyExtractor={(item, index) => {
          return index.toString();
        }}
        renderItem={({item, index}) => {
          return (
            <RenderSimpsons
              key={index}
              item={item}
              onPress={() => props.navigation.navigate('SimpsonDetail', {item})}
            />
          );
        }}
      />
      <TouchableOpacity
        onPress={() => props.navigation.navigate('AddSimpson')}
        style={{
          position: 'absolute',
          width: 50,
          height: 50,
          bottom: 50,
          borderRadius: 25,
          right: 50,
          backgroundColor: 'lightblue',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image source={icons.plus} style={{tintColor: 'white'}} />
      </TouchableOpacity>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    simpsons: state.simpsons.simpsonsList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetList: () => dispatch(getList()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
