import trash from '../../assets/icons/trash.png';
import plus from '../../assets/icons/plus.png';

export const icons = {
  trash,
  plus,
};
