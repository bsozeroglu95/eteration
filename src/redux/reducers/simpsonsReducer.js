import _ from 'lodash';

import {GET_LIST, LIST_SAVE} from '../actions/simpsons';

const initialState = {
  simpsonsList: [],
};

const simpsonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST:
      return {
        ...state,
        simpsonsList: action.simpsonsList,
      };
    case LIST_SAVE:
      return {
        ...state,
        simpsonsList: action.simpsonsList,
      };
    default:
      return state;
  }
};

export default simpsonsReducer;
