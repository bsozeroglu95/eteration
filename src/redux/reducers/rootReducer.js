import {combineReducers} from 'redux';
import simpsonsReducer from './simpsonsReducer';

export default combineReducers({
  simpsons: simpsonsReducer,
});
