import {get} from '../../api/BaseRequest';

export const GET_LIST = 'GET_LIST';
export const LIST_SAVE = 'LIST_SAVE';

export const getList = () => {
  return async (dispatch) => {
    const data = await get({
      url: 'https://5fc9346b2af77700165ae514.mockapi.io/simpsons',
    });
    dispatch({
      simpsonsList: data,
      type: GET_LIST,
    });
  };
};

export const listSave = (data) => {
  return async (dispatch) => {
    dispatch({
      simpsonsList: data,
      type: LIST_SAVE,
    });
  };
};
