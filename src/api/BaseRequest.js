import React from 'react';
import {View, Text} from 'react-native';

export const get = async (params = {url}) => {
  const {url} = params;
  const response = await fetch(url, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'GET',
  });
  if (response.status) {
    var resData = await response.json();
    return resData;
  } else {
    throw new Error('Veriler alınırken hata');
  }
};
